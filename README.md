##Aperture
A material design gallery. A direct gateway to your photos. 
===========================================================
Based (with permission) off of Impression by Aidan Follestad (@afollestad), Aperture was developed for the purpose of making a beautiful, yet non-
distracting Photo Gallery. Right now, it is still in BETA, but more features are coming soon, including Google Drive and Dropbox
intergration, and a photo editor. 

===========================================================
Download: https://play.google.com/store/apps/details?id=com.marlonjones.aperture

ALPHA Test (Test features before going to BETA): https://play.google.com/apps/testing/com.marlonjones.aperture

Community: https://plus.google.com/u/0/communities/116518857170051710391


===========================================================
Contribution to this project is greatly welcomed. Also, please check out the original source code, Impression.
NOTE: THIS PROJECT WILL NOT BE RECIVING MANY MORE UPDATES, AS THE CODE WILL BE MERGED WITH IMPRESSION. 

#Licensed under GPLv3
 
Copyright (c) 2015 Marlon Jones

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.



